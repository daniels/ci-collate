# ci-collate

This module has been created while realizing we are doing the same or a very 
similar task from different tools. So, better if we provide a stand-alone tool
to do this task in a way we can use from multiple points as well as other 
projects may benefit.

## Usage

Use `ci-collate --help` for further information, but here you could see some 
examples:

### Single job request

Using the token stored in the environment variable `GITLAB_TOKEN`, one can 
request in the stdout the trace of a job. For example, the job's output of
`https://gitlab.freedesktop.org/gfx-ci-bot/mesa/-/jobs/37243489` can be 
requested by:
```commandline
ci-collate --namespace gfx-ci-bot job --trace 37243489
```
It didn't include `--project mesa` or 
`--gitlab-url https://gitlab.freedesktop.org/` because those are the default 
values.

The same example, but from a python code:

```python
from collate import Collate
Collate(namespace='gfx-ci-bot').from_job(37243489).trace()
```

To get the `failures.csv` file from this job:
```commandline
ci-collate --namespace gfx-ci-bot job --artifact results/failures.csv 37243489
```

And from python

```python
from collate import Collate
Collate(namespace='gfx-ci-bot').from_job(37243489).get_artifact('results/failures.csv')
```

### Pipeline request

Similar to `job` subcommand:

```commandline
ci-collate pipeline --artifact results/failures.csv 819067
```

And from python:

```python
from collate import Collate
Collate(namespace='gfx-ci-bot').from_pipeline(819067).get_artifact('results/failures.csv')
```

With pipelines some filtering can be setup base on _regex_, their _state_ or 
_status_.

```python
from pprint import pprint
from collate import Collate
pipeline = Collate(namespace='sergi', project_name='virglrenderer').from_pipeline(820983)
pprint(pipeline.get_artifact(artifact_name="results/junit.xml", job_regex=r"^piglit.*virt"))
```
Will return:
```
{'piglit-gl-virt 1/3': '<?xml version="1.0" encoding="utf-8"?>\n'
                       '<testsuites>\n'
                       '  <testsuite id="0" name="gpu" package="testsuite/gpu" '
                       'tests="0" errors="0" failures="0" hostname="localhost" '
                       'timestamp="2023-03-03T09:58:19.215052909+00:00" '
                       'time="0" />\n'
                       '</testsuites>',
 'piglit-gl-virt 2/3': '<?xml version="1.0" encoding="utf-8"?>\n'
                       '<testsuites>\n'
                       '  <testsuite id="0" name="gpu" package="testsuite/gpu" '
                       'tests="0" errors="0" failures="0" hostname="localhost" '
                       'timestamp="2023-03-03T10:02:04.360507348+00:00" '
                       'time="0" />\n'
                       '</testsuites>',
 'piglit-gl-virt 3/3': '<?xml version="1.0" encoding="utf-8"?>\n'
                       '<testsuites>\n'
                       '  <testsuite id="0" name="gpu" package="testsuite/gpu" '
                       'tests="0" errors="0" failures="0" hostname="localhost" '
                       'timestamp="2023-03-03T09:59:44.446455139+00:00" '
                       'time="0" />\n'
                       '</testsuites>',
 'piglit-gles-virt 1/3': '<?xml version="1.0" encoding="utf-8"?>\n'
                         '<testsuites>\n'
                         '  <testsuite id="0" name="gpu" '
                         'package="testsuite/gpu" tests="0" errors="0" '
                         'failures="0" hostname="localhost" '
                         'timestamp="2023-03-03T10:00:11.660667208+00:00" '
                         'time="0" />\n'
                         '</testsuites>',
 'piglit-gles-virt 2/3': '<?xml version="1.0" encoding="utf-8"?>\n'
                         '<testsuites>\n'
                         '  <testsuite id="0" name="gpu" '
                         'package="testsuite/gpu" tests="0" errors="0" '
                         'failures="0" hostname="localhost" '
                         'timestamp="2023-03-03T10:02:39.617946666+00:00" '
                         'time="0" />\n'
                         '</testsuites>',
 'piglit-gles-virt 3/3': '<?xml version="1.0" encoding="utf-8"?>\n'
                         '<testsuites>\n'
                         '  <testsuite id="0" name="gpu" '
                         'package="testsuite/gpu" tests="0" errors="0" '
                         'failures="0" hostname="localhost" '
                         'timestamp="2023-03-03T10:01:12.625760845+00:00" '
                         'time="0" />\n'
                         '</testsuites>'}
```