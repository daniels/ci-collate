#!/usr/bin/env python3.9

# Copyright (C) 2023 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2023 Collabora Ltd"

from gitlab.exceptions import GitlabGetError
from gitlab.v4.objects.jobs import ProjectJob
from tenacity import retry, stop_after_attempt, wait_exponential
from typing import Union


class CollateJob:
    # TODO: document methods
    __gl_job: ProjectJob = None

    def __init__(self, gitlab_job: ProjectJob):
        """
        Wrapper to a gitlab job with methods to manage the access to its trace
        and the artifacts.
        :param gitlab_job: gitlab job object to wrap
        """
        self.__gl_job = gitlab_job

    def __str__(self):
        return self.name

    def __repr__(self):
        return f"CollateJob({self})"

    @property
    def name(self):
        return self.__gl_job.name

    @property
    def id(self):
        return self.__gl_job.id

    @property
    def web_url(self):
        return self.__gl_job.web_url

    @property
    def stage(self):
        return self.__gl_job.stage

    @property
    def status(self):
        return self.__gl_job.status

    @property
    def attributes(self):
        return self.__gl_job.attributes

    def trace(self) -> str:
        """
        Query the traces log of the job.
        :return: string with the job trace log
        """
        if trace := self.__get_trace():
            return trace
        raise FileNotFoundError

    # TODO: list the available artifacts

    def get_artifact(self, artifact_name) -> str:
        """
        Query the artifact file of the job.
        :param artifact_name:
        :return: string with the content of the file interpreted as string
        """
        if artifact := self.__get_artifact(artifact_name):
            return artifact
        raise FileNotFoundError

    @retry(stop=stop_after_attempt(6), wait=wait_exponential(multiplier=1))
    # retry waiting 1, 2, 4, 8, 16, 32 = 63s
    def __get_trace(self) -> Union[str, None]:
        try:
            return self.__gl_job.trace().decode('UTF-8')
        except GitlabGetError as exception:
            if exception.response_code == 404:
                return None

    @retry(stop=stop_after_attempt(6), wait=wait_exponential(multiplier=1))
    # retry waiting 1, 2, 4, 8, 16, 32 = 63s
    def __get_artifact(self, artifact_name) -> Union[str, None]:
        try:
            return self.__gl_job.artifact(artifact_name).decode('UTF-8')
        except GitlabGetError as exception:
            if exception.response_code == 404:
                return None
