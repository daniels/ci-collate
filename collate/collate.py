#!/usr/bin/env python3.9

# Copyright (C) 2023 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2023 Collabora Ltd"

from .defaults import (default_gitlab_url, default_gitlab_token_file_name,
                       default_project_name)
from gitlab import Gitlab
from gitlab.exceptions import GitlabGetError
from gitlab.v4.objects.jobs import ProjectJob
from gitlab.v4.objects.pipelines import ProjectPipeline
from gitlab.v4.objects.projects import Project
from .job import CollateJob
import os
from .pipeline import CollatePipeline
from tenacity import retry, stop_after_attempt, wait_exponential
from typing import Union


class Collate:
    __gitlab_url: str = None
    __token_file: str = None
    __namespace: str = None
    __project_name: str = None

    __gl: Gitlab = None
    __project: Project = None

    def __init__(self, gitlab_url=None,
                 gitlab_token_file_name=default_gitlab_token_file_name,
                 namespace=None, project=None):
        """
        Bind to a gitlab project in a given namespace
        :param gitlab_url: string of the gitlab server url
        :param gitlab_token_file_name: file name where a token is stored
        :param namespace: namespace of the gitlab server where the project is
        :param project: project of the gitlab server to bind
        """
        if not gitlab_url:
            gitlab_url = default_gitlab_url
        self.__gitlab_url = gitlab_url
        self.__token_file = os.path.expanduser(gitlab_token_file_name)
        self.__build_gitlab_object()
        self.__namespace = self.__gl.user.username \
            if namespace is None else namespace
        self.__project_name = project \
            if project is not None else default_project_name
        self.__build_project_object()
        # print(f"Collate object build for {self.__project.web_url}")

    @property
    def path_with_namespace(self):
        return f"{self.__namespace}/{self.__project_name}"

    def from_job(self, idn: int) -> CollateJob:
        """
        Get a CollateJob object, wrapper of the gitlab job.
        :param idn: number identifying the job
        :return: job wrapper
        """
        gl_job = self.__get_job_obj(idn)
        if gl_job is None:
            raise AssertionError(f"Cannot bind with {idn} job in "
                                 f"{self.__namespace}/{self.__project_name} "
                                 f"on {self.__gitlab_url}")
        return CollateJob(gl_job)

    def from_pipeline(self, idn: int) -> CollatePipeline:
        """
        Get a CollatePipeline object, wrapper of the gitlab pipeline
        :param idn: number identifying the pipeline
        :return: pipeline wrapper
        """
        gl_pipeline = self.__get_pipeline_obj(idn)
        if gl_pipeline is None:
            raise AssertionError(f"Cannot bind with {idn} pipeline in "
                                 f"{self.__namespace}/{self.__project_name} "
                                 f"on {self.__gitlab_url}")
        pipeline_jobs = []
        for job in gl_pipeline.jobs.list(iterator=True):
            gl_job = self.__get_job_obj(job.id)
            pipeline_jobs.append(CollateJob(gl_job))
            # print(f"Build job {job.name} ({job.id}) from pipeline {idn}")
        return CollatePipeline(gl_pipeline, pipeline_jobs)

    def __build_gitlab_object(self) -> None:
        """
        Build the link to the gitlab server to collate information about jobs
        and pipelines
        :return: None
        """
        gitlab_token = os.environ.get('GITLAB_TOKEN')
        if gitlab_token is None:
            try:
                with open(self.__token_file) as file_descriptor:
                    gitlab_token = file_descriptor.read().strip()
            except FileNotFoundError:
                raise AssertionError(
                    f"Use the GITLAB_TOKEN environment variable or "
                    f"the ~/.gitlab-token to provide this information")
        self.__gl = self._gitlab_builder(url=self.__gitlab_url,
                                         private_token=gitlab_token)
        self.__gl.auth()

    def __build_project_object(self) -> None:
        """
        Build the path with namespace to the project and bind the gitlab
        project object.
        :return: None
        """
        path_with_namespace = f"{self.__namespace}/{self.__project_name}"
        self.__project = self.__get_project_obj(path_with_namespace)
        if self.__project is None:
            raise AssertionError(f"Cannot bind with {path_with_namespace} "
                                 f"on {self.__gitlab_url}")

    @staticmethod
    @retry(stop=stop_after_attempt(6), wait=wait_exponential(multiplier=1))
    # retry waiting 1, 2, 4, 8, 16, 32 = 63s
    def _gitlab_builder(url, private_token=None) -> Gitlab:
        return Gitlab(url, private_token=private_token)

    @retry(stop=stop_after_attempt(6), wait=wait_exponential(multiplier=1))
    # retry waiting 1, 2, 4, 8, 16, 32 = 63s
    def __get_project_obj(self, path_with_namespace) -> Union[Project, None]:
        try:
            return self.__gl.projects.get(path_with_namespace)
        except GitlabGetError as exception:
            if exception.response_code == 404:
                return None

    @retry(stop=stop_after_attempt(6), wait=wait_exponential(multiplier=1))
    # retry waiting 1, 2, 4, 8, 16, 32 = 63s
    def __get_job_obj(self, job_id) -> Union[ProjectJob, None]:
        try:
            return self.__project.jobs.get(job_id)
        except GitlabGetError as exception:
            if exception.response_code == 404:
                return None

    @retry(stop=stop_after_attempt(6), wait=wait_exponential(multiplier=1))
    # retry waiting 1, 2, 4, 8, 16, 32 = 63s
    def __get_pipeline_obj(self, pipeline_id) -> Union[ProjectPipeline, None]:
        try:
            return self.__project.pipelines.get(pipeline_id)
        except GitlabGetError as exception:
            if exception.response_code == 404:
                return None
