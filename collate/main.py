#!/usr/bin/env python3.9

# Copyright (C) 2023 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2023 Collabora Ltd"

from argparse import ArgumentParser, Namespace
from .collate import Collate
from .defaults import (default_gitlab_url, default_gitlab_token_file_name,
                       default_project_name)
from gitlab.exceptions import GitlabAuthenticationError


def main():
    args = __cli_arguments()
    try:
        collate = Collate(gitlab_url=args.gitlab_url, namespace=args.namespace,
                          project_name=args.project)
        if args.subcommand == 'job':
            job = collate.from_job(args.JOB_ID)
            if args.trace:
                print(job.trace())
            if args.artifact:
                print(job.get_artifact(args.artifact))
        elif args.subcommand == 'pipeline':
            pipeline = collate.from_pipeline(args.PIPELINE_ID)
            if args.artifact:
                print(pipeline.get_artifact(args.artifact))
    except AssertionError as exception:
        print(f"Cannot proceed due to: {exception}")
    except GitlabAuthenticationError as exception:
        print(f"Review credentials: {exception}")


def __cli_arguments() -> Namespace:
    """
    Define the command line behavior for the arguments and subcommands.
    :return: argument parser
    """
    parser = ArgumentParser(
        description="CLI tool to gather information about Gitlab jobs output "
                    "and artifact. Individually or as members of a pipeline.")
    # General arguments:
    parser.add_argument("--gitlab-url", metavar="URL",
                        help=f"Url addres where the gitlab server is. "
                             f"Default {default_gitlab_url!r}")
    parser.add_argument("--namespace", metavar="NAME",
                        help=f"Personal or group namespace to locate a gitlab "
                             f"project. If not specified it will be used the "
                             f"user name from the gitlab token.")
    parser.add_argument("--project", metavar="PROJECT",
                        help=f"Project on the gitlab server where the "
                             f"information will be lookup. Default "
                             f"{default_project_name!r}")
    # Subcommands:
    subparsers = parser.add_subparsers(title='subcommands', dest='subcommand',
                                       description='valid subcommands',
                                       help='sub-command additional help')
    # collate single job
    parser_job = subparsers.add_parser(
        'job', help="Collate from an existing job.")
    parser_job.add_argument(
        "JOB_ID", help="Identify the job to collate.")
    parser_job.add_argument("--trace", action='store_true',
                            help="Print in stdout the job trace.")
    parser_job.add_argument("--artifact", metavar="PATH",
                            help="Print in stdout the artifact content")
    # collate from a pipeline
    parser_pipeline = subparsers.add_parser(
        'pipeline', help="Collate from an existing pipeline.")
    parser_pipeline.add_argument(
        "PIPELINE_ID", help="Identify the pipeline to collate.")
    parser_pipeline.add_argument("--artifact", metavar="PATH",
                                 help="Print in stdout the artifact content")
    # TODO: filters, like stage or status of the jobs
    parser.epilog = f"Note: " \
                    f"Use GITLAB_TOKEN or {default_gitlab_token_file_name} " \
                    f"to provide the token with access to the project you " \
                    f"want to access."
    return parser.parse_args()


if __name__ == '__main__':
    main()
