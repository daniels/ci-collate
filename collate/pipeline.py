#!/usr/bin/env python3.9

# Copyright (C) 2023 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2023 Collabora Ltd"

from collections import defaultdict, deque
from .job import CollateJob
from gitlab.v4.objects.pipelines import ProjectPipeline
from re import match as re_match
from typing import Union


class CollatePipeline:
    # TODO: document methods
    __gl_pipeline: ProjectPipeline = None
    __all_jobs: deque = None
    __jobs_by_stage: dict = None

    def __init__(self, gitlab_pipeline: ProjectPipeline, collate_jobs: list):
        """
        Represents a pipeline in a gitlab server providing information and an
        interface to collate the results.
        :param gitlab_pipeline: gitlab pipeline object to wrap
        :param collate_jobs: wrapped jobs in the pipeline
        """
        self.__gl_pipeline = gitlab_pipeline
        self.__all_jobs = deque()
        self.__jobs_by_stage = defaultdict(deque)
        for job in collate_jobs:
            # print(f"organizing job {job.name} -> "
            #       f"stage{job.stage} status:{job.status}")
            self.__all_jobs.append(job)
            self.__jobs_by_stage[job.stage].append(job)

    def __str__(self):
        return f"{self.id}"

    def __repr__(self):
        return f"CollatePipeline({self})"

    @property
    def id(self):
        return self.__gl_pipeline.id

    @property
    def web_url(self):
        return self.__gl_pipeline.web_url

    @property
    def status(self):
        return self.__gl_pipeline.status

    @property
    def attributes(self):
        return self.__gl_pipeline.attributes

    @property
    def pipeline_stages(self) -> set:
        """
        Query which stages are in the pipeline.
        :return: Set of stages in the pipeline.
        """
        return set(self.__jobs_by_stage.keys())

    @property
    def job_stata(self) -> set:
        """
        Query about the status that the jobs in the pipeline has.
        :return: Set of job status currently in the pipelin
        """
        return set((job.status for job in self.__all_jobs))

    def jobs_in_stage(self, stage: Union[str, list, set]) -> set:
        """
        Query the jobs, filtered by belonging to a stage or a set of them.
        :param stage: a stage or a list of them
        :return: set jobs in the stages in the pipeline
        """
        return self.__query_dictionary_keys(stage, self.__jobs_by_stage)

    def jobs_in_status(self, status: Union[str, list]) -> set:
        """
        Query the jobs that have the status filtered by the argument.
        :param status: a status or a list of them
        :return: set of jobs currently in the requested status in the pipeline
        """
        jobs_by_status = defaultdict(deque)
        for job in self.__all_jobs:
            jobs_by_status[job.status].append(job)
        return self.__query_dictionary_keys(status, jobs_by_status)

    def trace(self,
              job_regex: str = None,
              status: Union[str, list, set] = None,
              stage: Union[str, list, set] = None,
              ) -> dict:
        """
        Query the trace for a set of jobs. Many arguments allow to subset the
        jobs in the pipeline in many ways.
        :param job_regex: regular expression to filter the jobs in the pipeline
        :param status: status or a list of them to filter the jobs
        :param stage: stage or a list of them to filter the jobs
        :return: dictionary with the trace on each of the jobs
        """
        return self.__job_selection(
            "trace", job_regex=job_regex, status=status, stage=stage)

    # TODO: list the available artifacts

    def get_artifact(
            self, artifact_name,
            job_regex: str = None,
            status: Union[str, list, set] = None,
            stage: Union[str, list, set] = None,
            ) -> dict:
        """
        Query artifacts for a set of jobs. Many arguments allow to subset the
        jobs in the pipeline in many ways.
        :param artifact_name: string with the name
        :param job_regex: regular expression to filter the jobs in the pipeline
        :param status: status or a list of them to filter the jobs
        :param stage: stage or a list of them to filter the jobs
        :return: dictionary with the artifact on each of the jobs
        """
        # TODO: the paths in virglrenderer have an intermediate subdirectory:
        #  results/{deqp,piglit}-{gl,gles}-{host,virt}/results.csv
        return self.__job_selection(
            "get_artifact", args_list=[artifact_name],
            job_regex=job_regex, status=status, stage=stage)

    def __job_selection(
            self,
            action: str, args_list: list = None, args_dict: dict = None,
            job_regex: str = None,
            status: Union[str, list, set] = '*',
            stage: Union[str, list, set] = '*',
            ) -> dict:
        """
        Filter the jobs in the pipeline by their name using a regular expression
        to then call a specific action for each of them and summarize the
        results with a dictionary structure.
        :param action: string with the name of a job object method
        :param args_list: args to the action
        :param args_dict: kwargs to the action
        :param job_regex: regular expression to filter jobs
        :param status: status of the jobs to include in the action
        :param stage: stage of the jobs to include in the action
        :return: dictionary with the requested information by job
        """
        args_list = [] if args_list is None else args_list
        args_dict = {} if args_dict is None else args_dict
        output = {}
        requests_jobs = self.jobs_in_status(
            status).intersection(self.jobs_in_stage(stage))
        for job in requests_jobs:
            if job_regex is not None:
                if not re_match(job_regex, job.name):
                    continue
            try:
                if hasattr(job, action):
                    caller = getattr(job, action)
                    artifact = caller(*args_list, **args_dict)
                    # print(f"For {job.name} get {args_list}/{args_dict} "
                    #       f"artifact (length {len(artifact)})")
                    output[job.name] = artifact
            except FileNotFoundError as exception:
                # print(f"For {job.name} file not found")
                output[job.name] = f"{exception}"
        return output

    @staticmethod
    def __query_dictionary_keys(
            query: Union[str, list, set], dct: dict
            ) -> set:
        """
        One would like to subset the jobs by one or moy keys, or even get all
        of them. This method allows to create a single list of elements with
        the jobs satisfying the condition to be in the keys of the query.
        :param query: can be a single name or a set of them
        :param dct: the dictionary from where the values are listed.
        :return: list of elements that satisfies the conditions.
        """
        if isinstance(query, str):
            if query == '*':
                lst = []
                for value in dct.values():
                    lst += list(value)
                return set(lst)
            if query in dct.keys():
                return set(dct[query])
            raise KeyError(f"Review the available keys. {query!r} isn't there.")
        if isinstance(query, list):
            query = set(query)
        if isinstance(query, set):
            available_values = set(dct.keys())
            if not query.issubset(available_values):
                invalid = query.difference(available_values)
                raise KeyError(f"Review the available keys. {invalid} "
                               f"{'is' if len(invalid) == 1 else 'are'}n't "
                               f"there.")
            lst = []
            for element in query:
                lst += list(dct[element])
            return set(lst)
