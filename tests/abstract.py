#!/usr/bin/env python3.9

# Copyright (C) 2023 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2023 Collabora Ltd"

from collate import Collate
from os import environ as os_environ
from logging import Logger, StreamHandler, DEBUG
from mockgitlab import create_random_fake_token, Gitlab
from unittest import TestCase


def _gitlab_builder(self, url, private_token=None):
    return Gitlab(url, private_token=private_token)


class CollateTestCase(TestCase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def _prepare_logger(self, name: str):
        self._logger = Logger(f"collate_{name}")
        channel = StreamHandler()
        self._logger.addHandler(channel)
        self._logger.setLevel(DEBUG)

    def _prepare_environment(self, environment: dict):
        for variable, value in environment.items():
            self._logger.debug(f"{variable=}: {value=}")
            os_environ[variable] = value

    def _build_collate_object(self) -> Collate:
        fake_token = create_random_fake_token()
        self._prepare_environment({'GITLAB_TOKEN': fake_token})
        # force to create the build of the fake
        Collate._gitlab_builder = _gitlab_builder
        return Collate()
