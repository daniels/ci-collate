#!/usr/bin/env python3.9

# Copyright (C) 2023 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2023 Collabora Ltd"

from abstract import CollateTestCase
from collate.pipeline import CollatePipeline


class CollatePipelineTestCase(CollateTestCase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._prepare_logger("TestCollatePipelineFailuresArtifact")

    def __prepare_fake_pipeline(self) -> CollatePipeline:
        collate = self._build_collate_object()
        project = collate._Collate__gl.projects.get(collate.path_with_namespace)
        fake_pipeline = project.pipeline.get(654321)
        # TODO: prepare the gitlab mocked pipeline to return the information
        #  like an original one does to then access this information from
        #  collate objects.
        return collate.from_pipeline(fake_pipeline.id)

    # def test_collate_pipeline_trace(self):
    #     pipeline = self.__prepare_fake_pipeline()
    #     # self.assertEqual(job.trace(), "Job success")

    # def test_collate_job_artifact(self):
    #     job = self.__prepare_fake_job()
    # TODO    self.assertEqual(job.get_artifact('results/failures.csv'), "")
