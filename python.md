# Python environment

## Python using virtualenv

Based on python 3.9, one can setup a virtual environments. 

```bash
$ pip3 -V
pip 20.3.4 from /usr/lib/python3/dist-packages/pip (python 3.9)
$ python3 -m pip install virtualenv
```

A virtual environment can be created in order to use this module: 

```bash
$ python3 -m virtualenv ci-collate.venv
$ source ci-collate.venv/bin/activate
(...)
$ python -m pip install python-gitlab ruamel.yaml tenacity
$ pip list
Package            Version
------------------ ---------
certifi            2022.12.7
charset-normalizer 3.0.1
idna               3.4
pip                23.0.1
python-gitlab      3.13.0
requests           2.28.2
requests-toolbelt  0.10.1
ruamel.yaml        0.17.21
ruamel.yaml.clib   0.2.7
setuptools         67.0.0
tenacity           8.2.1
urllib3            1.26.14
wheel              0.38.4
```

To reproduce the same virtual environment that this tools is being tested:

```bash
$ pip install -r requirements.txt
```

### running tests

To run the pytests, one extra dependency is needed that is under development
```
$ pip install pytest 
$ pip install git+https://gitlab.freedesktop.org/sergi/python-gitlab-mock 
$ pip install -e . 
```

### Export changes on the environment

In case some newer packages are required, the easiest way to mark for others 
to use is:

```bash
pip freeze --all > requirements.txt
```

